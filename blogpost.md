## NaN

NaN is a value representing Not-a-Number

NaN is a variable in the global scope.In the beginning, the `NaN` is Not-a-Number. But in recent browsers, `NaN` is a non-configurable and non-writable property. And usage of the `NaN` is very limited and rare. Because `NaN` is non-configurable and non-writable don't override the
`Nan`

There are some operations in which `NaN` is returned

1. String inside number and parse that variable

``` let number = "setgb" parse(number) Or Number(number) ```

in this method, the output will return as a `NaN`


2. Using of Math operation when the variable or result is actually is not a number

``` Let result = Math.Pi(f)``` or ``` Math.sqrt(rt)```

in this case, also result returned as a `NaN`

3. Using NaN to operate other variables or results also results in `NaN`
``` let result = 5+NaN+10*NaN```

In this case, also returned value results as `NaN`

4. Indeterminate form operating NaN or undefined value to perform operation results in `NaN`

5. Involving any string operator to perform some task

```blogpost/52``` ``num*100``

`Note` usage of `+` operator results in concatenating string so this is not valid here

### examples of NaN
``` NaN === NaN ``` 

this will return as false

``` function valueIsNaN(v) {
     return v !== v; 
     }
valueIsNaN(1); 
```

The above operation will return a false value because we pass an integer and that is a number so in return it'll return as false value

all methods in js cannot find 
for example:
`let arr=[4,56,NaN,58]`
`indexOf` method will not work in finding NaN

## Number.isNaN

The Number.isNaN method checks that the passed value is `NaN` and its data type is Number
this method is an advanced method of finding isNaN

#### syntax

Number.isNaN(value)
#### examples
```
let value = 80RR
if (Number.isNaN(value)) {
    return 'Number NaN';
  }
  else{
    return NaN
  }
```

here in above enter return true if passed value is NaN and its data type is Number

Otherwise, it returns as a false value

usage of `Number.isNaN` become necessary when equality operators such as
`==`  `===` these operators evaluate to false when checking if NaN is NaN
that's Number.isNaN method is used here. 

`value !== value` is true when NaN among all possible values in JavaScript
`Number.isNaN(value)` can replace for above method to get accurate or strictly check

compared to isNaN() and Number.isNaN won't affect when converting a number from one parameter
this can be easy and safe to pass the value as NaN and its type is a number and returned value will be true


#### more examples

``Number.isNaN(123)``

The data type of 123 is a number but 123 is not equal to NaN, so it returns as false.


`Number.isNaN('123')`
    
the datatype of 123 is string & '123' is not equal to NaN, so it returns false.


`Number.isNaN(NaN) `

The data type of NaN is a number & NaN is equal to NaN, that's why it returns true.


`Number.isNaN('NaN')`

The data type of 'NaN' is a string & 'NaN' is not equal to NaN, that's why it returns false.

```
let x = 0;
let y = 0;
let result = Number.isNaN(x/y)
```
here above it returns as a true value

`let value = "rarg"`
`Number.isNaN(value)`

here in the above code, it returns as a false value because of type of value is not a number

```Number.isNaN(37);```

in this value returns as a false value because the entered value is a proper integer

`Number.isNaN('37');`
in this also it returns as false entered value is the string 

## Difference between isNan and Number.isNaN

`isNaN` is trying to convert the passed argument to a Number
and returns the resulting value is `NaN`

but `Number.isNaN` doesn't convert the argument, and it directly returns argument is a Number and is NaN


`isNaN()` will check if the converted value to a number data type

for example
`sse`

`Number.isNaN()` this method will check if the entered value type is a number but entered value is not valid

for example:
``` 
let value1 = bb;
let value2 = 33;
let result = value1/value2
```

`Number.isNaN()` introduced in ES6 version

the `isNaN()` is aglobal function
the `Number.isNaN()` is a method on Number object

`isNaN` implementation treats string value as the number and returns the result based on this.
to solve this misconception we have the method `Number.isNaN`

`isNaN` It checks if the value passed to it is NaN or
    It will try to force the passed value into a number if the result is NaN then the return value will be true

`Number.isNaN`   It checks if the current value passed in actually NaN